<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="./CSS/stylesheet.css" rel="stylesheet" type="text/css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー編集画面</title>
		<link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>

	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="./setting" method="post"><br />

			<a href="./management">ユーザー管理</a> <br />

			<input name="userId" value="${user.id}" type="hidden"/>

			<label for="account">アカウント</label>
			<input name="account" value="${user.account}" id="account" /> <br />

			<label for="password">パスワード</label>
			<input name="password" type="password" id="password" /> <br />

			<label for="password2">確認用パスワード</label>
			<input name="password2" type="password" id="password2" /> <br />

			<label for="name">名前</label>
			<input name="name" value="${user.name}" id="name" /> <br />

			<c:if test="${ loginUser.id != user.id }">
				<label >支社</label>
					<select name="branchId" size="1" id= "branchId">
						<c:forEach items="${branches}" var="branch">

						<%--支社で設定しているidとsignupservletのgetuserのbranchが一致している場合 --%>
							<c:if test="${branch.id == user.branch }">
								<option value ="${branch.id}" selected>${branch.name}</option>
							</c:if>
							<c:if test="${branch.id != user.branch }">
								<option value ="${branch.id}">${branch.name}</option>
							</c:if>

						</c:forEach>

					</select> <br />
			</c:if>

			<%--ログインしてる人と変更しようとしているユーザーが一緒の場合支社が変更できない--%>
			<c:if test="${ loginUser.id == user.id }">
				<label for = "branches">支社</label>
					<select name = "branchId">
						<c:forEach items = "${branches}" var = "branch">
							<c:if test = "${branch.id == user.branch}">
						<option selected value = "${branch.id}">${branch.name}</option>
							</c:if>
						</c:forEach>
					</select><br>
			</c:if>


			<c:if test="${ loginUser.id != user.id }">
				<label >部署</label>
				<select name="departmentId" size="1" id="departmentId">
					<c:forEach items="${departments}" var="department">

					<%--部署で設定しているidとsignupservletのgetuserのdepartmentが一致している場合 --%>
						<c:if test="${department.id == user.department }">
							<option value ="${department.id}" selected>${department.name}</option>
						</c:if>
						<c:if test="${department.id != user.department }">
							<option value ="${department.id}">${department.name}</option>
						</c:if>

					</c:forEach>

				</select><br />
			</c:if>

			<%--ログインしてる人と変更しようとしているユーザーが一緒の場合部署が変更できない--%>
			<c:if test="${ loginUser.id == user.id }">
				<label for = "departments">部署</label>
				<select name = "departmentId">
					<c:forEach items = "${departments}" var = "department">
						<c:if test = "${department.id == user.department}">
							<option selected value = "${department.id}">${department.name}</option>
						</c:if>
					</c:forEach>
				</select><br>
			</c:if>

			<input type = "hidden" name = "id" value = "${user.id}">
			<input type = "hidden" name = "confirmAccount" value = "${user.account}">
			<input type="submit" value="更新" /> <br />
			<a href="./management">戻る</a>

		</form>
		<div class="copyright">Copyright(c)Yusuke Tsukamoto</div>
	</div>
</body>
</html>