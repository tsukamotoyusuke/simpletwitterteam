<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="./CSS/stylesheet.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー新規登録</title>
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>

		<form action="signup" method="post"><br />

			<a href="management">ユーザー管理</a> <br />


			<label for="account">アカウント</label>
			<input name="account" id="account" value="${user.account}" /> <br />

			<label for="password">パスワード</label>
			<input name="password" type="password" id="password" /> <br />

			<label for="password2">確認用パスワード</label>
			<input name="password2" type="password" id="password2" /> <br />

			<label for="name">名前</label>
			<input name="name" id="name" value="${user.name}"/> <br />

			<label>支社
				<select name="branch_id" size = "1" id="branch_id">

					<c:forEach items="${branches}" var="branch">
						<c:if test="${branch.id == branch_id}">
							<%--var branchに対しbeansのbranchesのidとname --%>
							<option value="${branch.id }"selected>${branch.name }</option>
						</c:if>
						<c:if test="${branch.id != branch_id}">
							<option value="${branch.id}"> ${branch.name}</option>
						</c:if>
					</c:forEach>

				</select> <br />
			</label>

			<label>部署
				<select name="department_id" size = "1" id="department_id">

					<c:forEach items="${departments}" var="department">
						<c:if test="${department.id == department_id}">
							<option value="${department.id }" selected>${department.name }</option>
						</c:if>
						<c:if test="${department.id != department_id}">
							<option value = "${department.id}">${department.name}</option>
						</c:if>
					</c:forEach>

				</select> <br />
			</label>

			<input type="submit" value="登録" /> <br />
			<a href="./management">戻る</a>

		</form>

		<div class="copyright">Copyright(c)Yusuke Tsukamoto</div>

	</div>

</body>
</html>