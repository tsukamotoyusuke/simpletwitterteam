<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="./CSS/stylesheet.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー管理画面</title>
		<script>
			function confirmStop(){

				if(window.confirm('変更しますか？')){
					message.check().submit();
					return true;

				} else {
					window.alert('変更をキャンセルされました');
					return false;
				}
										}
		</script>
</head>
<body>

	<div class="management">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope = "session"/>
		</c:if>

		<div class = "users">

				<a href="./">ホーム</a>
				<a href="./signup">ユーザー新規画面</a><br />

			<c:forEach items="${users}" var="user"><br />

				<div class="account">
					アカウント:<c:out value="${user.account}" />
				</div>

				<div class="name">
					名前:<c:out value="${user.name}" />
				</div>

				<div class="branchName">
					支社:<c:out value="${user.branchName}" />
				</div>

				<div class="departmentName">
					部署:<c:out value="${user.departmentName}" />
				</div>

				<label for="isStopped"></label>
				<div class="isStopped">
					アカウント復活停止状態:<c:out value="${user.isStopped}" />
				</div>

				<%--編集ボタンをおして編集したいユーザーを編集/編集を押すとSettingServletのdoGetメソッドへ--%>
				<form action="setting" method="get">
					<input name="userId" value="${user.id}" id="userId" type="hidden"/>
					<input type="submit" value="編集" />
				</form>

				<%--復活ボタンをおして復活したいユーザーを復活/復活を押すとStopServletのdoGetメソッドへ--%>
				<form action="stop" method="post">
					<c:if test = "${loginUser != user.id }">
						<c:if test="${user.isStopped == 0 }">
							<input name="userId" value="${user.id}" type="hidden"/>
							<input name="isStopped" value="1" type="hidden"/>
							<input type="submit" value="停止" onClick="confirmStop(); return false" />
						</c:if>

						<c:if test="${user.isStopped == 1 }">
							<input name="userId" value="${user.id}" type="hidden"/>
							<input name="isStopped" value="0" type="hidden"/>
							<input type="submit" value="復活" onClick="confirmStop(); return false" />
						</c:if>
					</c:if>
				</form>
			</c:forEach>

			<div class="copyright">Copyright(c)Yusuke Tsukamoto</div>

		</div>

	</div>
</body>
</html>
