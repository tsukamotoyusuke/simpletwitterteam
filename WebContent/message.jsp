<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<link href="./CSS/stylesheet.css" rel="stylesheet" type="text/css">
	<meta charset="UTF-8">
	<title>新規投稿画面</title>
</head>
<body>

	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>
	</div>

	<div class="form-area">

		<form action="message" method="post">
			<a href="">ホーム</a><br />

			<label for="title">件名</label><br />
			<input name="title" value="${title}" id="title" /> <br />

			<label for="category">カテゴリ</label><br />
			<input name="category" value="${category}" id="category" /> <br />

			<label for="text">投稿内容<br />
			<textarea name="text" cols="100" rows="5" class="tweet-box">${text}</textarea>
			</label> <br />

			<input type="submit" value="投稿"><br />
			<a href="./">戻る</a>
		</form>

		<div class="copyright">Copyright(c)Yusuke Tsukamoto</div>

	</div>

</body>
</html>