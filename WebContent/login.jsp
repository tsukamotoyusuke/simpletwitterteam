<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="./CSS/stylesheet.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ログイン画面</title>
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>

			<%--loginfilterで追加/ログイン以外にいったらセッションを切りエラーメッセージを他のページに行った際、表示できなくする --%>
			<c:remove var="errorMessages" scope = "session"/>
		</c:if>

		<form action="login" method="post"><br />

			<label for="account">アカウント名</label>
			<%--${account}とLoginservletのrequest.setAttribute("account", account);が繋がってるから一緒の名前にしてあげないと入力した情報が保持されない--%>
			<input name="account" value="${account}" id="account" /> <br />

			<label for="password">パスワード</label>
			<input name="password" type="password" id="password" /> <br />

			<input type="submit" value="ログイン" /> <br />

		</form>

		<div class="copyright">Copyright(c)Yusuke Tsukamoto</div>
	</div>
</body>
</html>