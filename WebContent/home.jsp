<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="./CSS/stylesheet.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム画面</title>
	<script>
		function confirmMessage(){

			if(window.confirm('削除しますか？')){

				message.check().submit();
				return true;
			}else{
				window.alert('削除キャンセルされました');
			return false;
			}
		}
	</script>
</head>
<body>

	<div class="main-contents">
		<div class="header">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<%--managementfilterで追加/セッションを切りエラーメッセージを他のページに行った際、表示できなくする --%>
				<c:remove var="errorMessages" scope = "session"/>
			</c:if>

			<a href="message">新規投稿</a>

			<c:if test = "${loginUser.branch == 1 }">
				<a href = "management">ユーザー管理</a>
			</c:if>

			<a href="logout">ログアウト</a>

			<div class="profile">
				<div class="account">アカウント名:<c:out value="${loginUser.account}" /></div>
				<div class="name">ログイン名:<c:out value="${loginUser.name}" /></div><br />
			</div>
		</div>

		<%--投稿絞り込み昨日で追加 --%>

		<form action="index.jsp">
				<label for="date">日付
					<%--${startTime}の内容はHomeServletのstart(ピンク)の情報をとってきている/type="date"でカレンダー表記になる--%>
					<input name="start" id="start" value = "${startTime}" type="date"/>～
					<input name="end" id="end" value = "${endTime}" type="date"/><br />

					<label for="">カテゴリ検索</label>
					<input name="category" id="category" value="${category}">

					<input type="submit" value="絞込み"><br />
				</label>
		</form>

		<a href="./">絞り込みリセット</a><br /><br />

		<div class="form-area">
			<c:forEach items="${messages}" var="message">

				<div class="name">
					ユーザー名:<c:out value="${message.name}" />
				</div>

				<div class="title">
					件名:<c:out value="${message.title}" />
				</div>

				<div class="category">
					カテゴリ:<c:out value="${message.category}" />
				</div><br />

				<label for="text">投稿内容</label>
				<div class="text">
					<%-- <pre></pre>で囲んだ内容は改行が反映される--%>
					<pre><c:out value="${message.text}" /></pre>
				</div>

				<label for="date">投稿日時</label>
				<div class="date">
					<fmt:formatDate value="${message.createdDate}"
						pattern="yyyy/MM/dd HH:mm:ss" />
				</div>

				<%--投稿削除で追加 --%>
				<c:if test="${loginUser.id == message.userId }">
					<form action="deleteMessage" method="post">
						<input type ="hidden" name = "messageId" value="${message.id}" >
						<input type="submit" value="投稿削除" onClick="confirmMessage(); return false" />
					</form>
				</c:if><br />

				<%--コメント表示で追加 --%>
				<c:forEach items="${comments}" var="comment">
					<c:if test="${comment.messageId == message.id }">

						<div class ="name">
							ユーザー名:<c:out value="${comment.name }"/>
						</div>

						<label>コメント内容</label>
						<div class ="commentText">
							<pre><c:out value="${comment.text }"/></pre>
						</div>

						<label>コメント日時</label>
						<div class="date">
							<fmt:formatDate value="${comment.createdDate}"
								pattern="yyyy/MM/dd HH:mm:ss" />
						</div>

						<%--コメント削除で追加 --%>
						<c:if test="${loginUser.id == comment.userId }">
							<form action="deleteComment" method="post"><br />

								<input name="commentId" value="${comment.id}" type="hidden"/>
								<input type="submit" value="コメント削除" onClick="confirmMessage(); return false" /><br />

							</form>
						</c:if>
					</c:if>
				</c:forEach><br />

				<%--コメント登録で追加 --%>
				<form action="comment" method="post">

					<label for="commentText">コメント入力欄<br />
						<textarea name="commentText" cols="100" rows="5" class="tweet-box">${commentText}</textarea>
					</label> <br />

					<input name="messageId" value="${message.id}" type="hidden"/>
					<input type="submit" value="コメント投稿"><br /><br />

				</form>

			</c:forEach>
		</div>
	</div>

	<div class="copyright">Copyright(c)Yusuke Tsukamoto</div>

</body>
</html>

