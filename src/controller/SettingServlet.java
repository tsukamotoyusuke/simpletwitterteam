package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branches;
import beans.Departments;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

		HttpSession session = request.getSession();

		String userId = request.getParameter("userId");

		if(StringUtils.isBlank(userId)) {
			errorMessages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("management");
			return;
		}

		if (!userId.matches("^[0-9]+$")){
			errorMessages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("management");
			return;
		}

		//ここのsettingとsetting.jspのif文が対応
		User user = new UserService().select(Integer.parseInt(request.getParameter("userId")));

		if(user== null) {
			errorMessages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("management");
			return;
		}
		request.setAttribute("user",user);

		List<Branches> branches = new BranchService().select();
		List<Departments> departments = new DepartmentService().select();
		request.setAttribute("branches",branches);
		request.setAttribute("departments",departments);

		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override		//management.jspの<form action="manegement" method="post"><br />のpost
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {


		List<String> errorMessages = new ArrayList<String>();
		List<Branches> branches = new BranchService().select();
		List<Departments> departments = new DepartmentService().select();


		//バリデーション表示で追加
		User user = getUser(request);
		if (!isValid(user, errorMessages, request)) {
			request.setAttribute("user", user);
			request.setAttribute("branches", branches);
			request.setAttribute("branchId", user.getBranch());
			request.setAttribute("departments", departments);
			request.setAttribute("departmentId", user.getDepartment());
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

		//↓の文がないとupdateされた情報が反映されず変更されない
		new UserService().update(user);

		//更新を押して遷移するとこ
		response.sendRedirect("./management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();

		user.setId(Integer.parseInt(request.getParameter("userId")));
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setPassword2(request.getParameter("password2"));
		int branch = Integer.parseInt(request.getParameter("branchId"));
		user.setBranch(branch);
		int department = Integer.parseInt(request.getParameter("departmentId"));
		user.setDepartment(department);

		return user;
	}

	//バリデーション表示で追加
	private boolean isValid(User user, List<String> errorMessages, HttpServletRequest request) {


		String account = user.getAccount();
		String password = user.getPassword();
		String password2 = user.getPassword2();
		String name = user.getName();
		int branch = user.getBranch();
		int department = user.getDepartment();

		User account2 = new UserService().select(user.getAccount());

		if(account2 != null ) {
			errorMessages.add("アカウントが重複しています");
		}

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウントを入力してください");
		} else if (!account.matches("^([0-9a-zA-Z]{6,20})$")) {
			errorMessages.add("アカウントは6文字以上20文字以下の半角英数字で入力してください");
		}

		if (StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力してください");
		} else if (!password.matches("^([0-9a-zA-Z]{6,20})$")){
			errorMessages.add("パスワードは6文字以上20文字以下の半角英数字で入力してください");
		}

		if (StringUtils.isBlank(password2)) {
			errorMessages.add("確認用パスワードを入力してください");
		} else if (!password2.equals(password) ) {
			errorMessages.add("パスワードと確認用パスワードが一致していません");
		}

		if (StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください");
		} else if (10 < name.length()) {
			errorMessages.add("名前は10文字以下で入力してください");
		}

		if (!(((branch == 1) && ((department == 1) || (department == 2))) || (((branch != 1) && ((department == 3) || (department == 4))))))  {
			errorMessages.add("支店と部署の組み合わせが正しくありません");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}


