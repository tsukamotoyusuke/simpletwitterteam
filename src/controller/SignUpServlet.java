package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branches;
import beans.Departments;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

	///signupのurlを動かすとサーブレットのsignup.jspにあるJSPファイルを読み込む	doGetを動かす
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<Branches> branches = new BranchService().select();
		// branches の情報をsignup.jspのitemsの branches に入れている
		request.setAttribute("branches", branches);

		List<Departments> departments = new DepartmentService().select();
		//departmentsの情報をsignup.jspのitemsのdepartmentsに入れている
		request.setAttribute("departments", departments);

		//request.get～～	→jspを呼び出す		サーブレットを動かして画面を表示する
		request.getRequestDispatcher("signup.jsp").forward(request, response);

	}

	@Override		//signup.jspの<form action="signup" method="post"><br />のpost
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//バリデーション表示で追加
		List<String> errorMessages = new ArrayList<String>();
		List<Branches> branches = new BranchService().select();
		List<Departments> departments = new DepartmentService().select();

		User user = getUser(request);

		if (!isValid(user, errorMessages, request)) {
			request.setAttribute("user", user);
			request.setAttribute("branches", branches);
			request.setAttribute("branch_id", user.getBranch());
			request.setAttribute("departments", departments);
			request.setAttribute("department_id", user.getDepartment());
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}

		new UserService().insert(user);
		response.sendRedirect("./management");
	}

	// getUserでUser.javaの情報を得ている
	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		//getParameterでparameterを得ている
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setPassword2(request.getParameter("password2"));
		user.setName(request.getParameter("name"));
		//"branch_id"は数字ではないため数字に変換するInteger.parseIntを使用
		int branch = Integer.parseInt(request.getParameter("branch_id"));
		user.setBranch(branch);
		int department = Integer.parseInt(request.getParameter("department_id"));
		user.setDepartment(department);
		return user;
	}


	//バリデーション表示で追加
	private boolean isValid(User user, List<String> errorMessages, HttpServletRequest request) {

		String account = user.getAccount();
		String password = user.getPassword();
		String password2 = user.getPassword2();
		String name = user.getName();
		int branch = user.getBranch();
		int department = user.getDepartment();

		//アカウントが重複していますのエラー表示で追加
		User user2 = new UserService().select(user.getAccount());

		if(user2 != null) {
			errorMessages.add("アカウントが重複しています");
		}

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウントを入力してください");
		} else if (!account.matches("^([0-9a-zA-Z]{6,20})$")) {
			errorMessages.add("アカウント名は6文字以上20文字以下の半角英数字で入力してください");
		}

		if (StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力してください");
		} else if (!password.matches("^([0-9a-zA-Z]{6,20})$")){
			errorMessages.add("パスワードは6文字以上20文字以下の半角英数字で入力してください");
		}

		if (StringUtils.isBlank(password2)) {
			errorMessages.add("確認用パスワードを入力してください");
		} else if (!password2.equals(password) ) {
			errorMessages.add("パスワードと確認用パスワードが一致していません");
		}

		if (StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください");
		} else if (10 <= name.length()) {
			errorMessages.add("名前は10文字以下で入力してください");
		}

		if (!(((branch == 1) && ((department == 1) || (department == 2))) || (((branch != 1) && ((department == 3) || (department == 4))))))  {
			errorMessages.add("支店と部署の組み合わせが正しくありません");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

}
