package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import beans.UserComment;
import service.CommentService;


//コメント登録で追加
@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		List<UserComment> comments = new CommentService().select();
		request.setAttribute("comments", comments);

		String text = request.getParameter("commentText");

		//バリデーションで追加
		if (!isValid(text, errorMessages)) {

			request.setAttribute("text", text);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("/home.jsp").forward(request, response);
			return;
		}

		Comment comment = new Comment();

		comment.setText(text);
		User user = (User) session.getAttribute("loginUser");
		comment.setUserId(user.getId());
		comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));


		new CommentService().insert(comment);
		response.sendRedirect("./");
	}


	//バリデーション表示で追加
	private boolean isValid(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("コメントを入力してください");
		} else if (500 <= text.length()) {
			errorMessages.add("コメントは500文字で入力してください");
		}
		if (errorMessages.size() != 0) {
			return false;
		}

		return true;

	}
}
