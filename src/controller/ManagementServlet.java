package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import beans.UserBranchDepartment;
import service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<UserBranchDepartment> management = new UserService().select();

		User loginUser = (User) request.getSession().getAttribute("loginUser");

		request.setAttribute("users", management);
		request.setAttribute("loginUser", loginUser.getId());
		request.getRequestDispatcher("/management.jsp").forward(request, response);
	}

}
