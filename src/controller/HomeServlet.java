package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//投稿絞り込みで追加
		String start = request.getParameter("start");
		String end = request.getParameter("end");
		String category = request.getParameter("category");

		List<UserMessage> messages = new MessageService().select(start, end , category);

		request.setAttribute("messages", messages);

		//投稿絞り込みで追加
		request.setAttribute("startTime", start);
		request.setAttribute("endTime", end);
		request.setAttribute("category", category);

		//コメント表示で追加
		List<UserComment> comments = new CommentService().select();
		request.setAttribute("comments", comments);
		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}
}


