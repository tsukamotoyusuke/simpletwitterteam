package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//request.getParameter("account")でユーザーが"acconut"で入力した情報を string accountに入れている
		String account = request.getParameter("account");
		String password = request.getParameter("password");

		//Serviceを経由してUserオブジェクトが取得できれば、そのオブジェクトをセッションにセットし、トップ画面へ遷移します。
		User user = new UserService().select(account, password);

		if (user == null) {
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("アカウントまたはパスワードが誤っています");

			request.setAttribute("account", account);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}


		//バリデーション表示で追加
		List<String> errorMessages = new ArrayList<String>();
		if (!isValid(user, errorMessages, account, password)) {

			request.setAttribute("account", account);
			request.setAttribute("password", password);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}


		HttpSession session = request.getSession();
		session.setAttribute("loginUser", user);
		response.sendRedirect("./");
	}


	//バリデーション表示で追加
	private boolean isValid(User user, List<String> errorMessages, String account, String password) {


		new UserService().updateIsStopped(user);


		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");
		}

		if (StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力してください");
		}

		if (user == null) {
			errorMessages.add("アカウントまたはパスワードが誤っています");
		}

		if(user.getIsStopped() == 1) {
			errorMessages.add("アカウントまたはパスワードが誤っています");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}

