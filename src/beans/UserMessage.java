package beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class UserMessage implements Serializable {

	private String account;
	private String name;
	private String title;
	private String category;
	private int id;
	private String text;
	private int userId;
	private Date createdDate;
	private Date updatedDate;
	private String startTime;
	private String endTime;

	public String getAccount() {
		return account;
	}
	public void setAccount(String parameter) {
		this.account = parameter;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}

	public void setId(int parameter) {
		this.id = parameter;
	}
	public int getId() {
		return id;
	}

	public void setText(String parameter) {
		this.text = parameter;
	}
	public String getText() {
		return text;
	}

	public void setUserId(int parameter) {
		this.userId = parameter;
	}
	public int getUserId() {
		return userId;
	}

	public void setCreatedDate(Timestamp parameter) {
		this.createdDate = parameter;
	}
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setUpdatedDate(Timestamp parameter) {
		this.updatedDate = parameter;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

}
