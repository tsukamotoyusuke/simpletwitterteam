package beans;

import java.util.Date;

public class Branches {

	private int id;
	private String name;
	private Date createdDate;
	private Date updatedDate;

	public int getId() {
		return id;
	}
	public void setId(int parameter) {
		this.id = parameter;
	}

	public String getName() {
		return name;
	}
	public void setName(String parameter) {
		this.name = parameter;
	}

	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date parameter) {
		this.createdDate = parameter;

	}

	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date parameter) {
		this.updatedDate = parameter;
	}

}
