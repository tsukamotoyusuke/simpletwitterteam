package beans;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {

	private int id;
	private String title;
	private String category;
	private String text;
	private int userId;;
	private Date createdDate;
	private Date updatedDate;

	public int getId() {
		return id;
	}
	public void setId(int parameter) {
		this.id = parameter;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String parameter) {
		this.title = parameter;
	}

	public String getCategory() {
		return category;
	}
	public void setCategory(String parameter) {
		this.category = parameter;
	}

	public String getText() {
		return text;
	}
	public void setText(String parameter) {
		this.text = parameter;
	}

	public int getUserId() {
		return userId;
	}
	public void setUserId(int parameter) {
		this.userId = parameter;
	}

	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}