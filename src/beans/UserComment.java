package beans;

import java.sql.Timestamp;
import java.util.Date;

public class UserComment {

	private String name;
	private int id;
	private String text;
	private int userId;
	private int messageId;
	private Date createdDate;
	private Date updatedDate;


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public void setId(int parameter) {
		this.id = parameter;
	}
	public int getId() {
		return id;
	}

	public void setText(String parameter) {
		this.text = parameter;
	}
	public String getText() {
		return text;
	}

	public void setUserId(int parameter) {
		this.userId = parameter;
	}
	public int getUserId() {
		return userId;
	}

	public void setMessageId(int parameter) {
		this.messageId = parameter;
	}
	public int getMessageId() {
		return messageId;
	}

	public void setCreatedDate(Timestamp parameter) {
		this.createdDate = parameter;
	}
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setUpdatedDate(Timestamp parameter) {
		this.updatedDate = parameter;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}

}
