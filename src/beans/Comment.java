package beans;

import java.io.Serializable;

public class Comment implements Serializable {

	private int userId;
	private int messageId;
	private String text;


	public int getUserId() {
		return userId;
	}
	public void setUserId(int parameter) {
		this.userId = parameter;
	}

	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int parameter) {
		this.messageId = parameter;
	}

	public String getText() {
		return text;
	}
	public void setText(String parameter) {
		this.text = parameter;
	}

}
