package beans;

import java.io.Serializable;
import java.util.Date;

//このファイルでSignUpServletのUserに情報を与える
public class User implements Serializable {

	private int id;
	private String account;
	private String name;
	private String password;
	private String password2;
	private int branchId;
	private int departmentId;
	private int isStopped;
	private Date createdDate;
	private Date updatedDate;

	//int型のparameterという引数をSetし{}内でparameterからthis.idでprivate int id;にしている。privateはSignUpServletのメソッド
	public void setId(int parameter) {
		this.id = parameter;
	}
	public int getId() {
		return id;
	}

	public void setAccount(String parameter) {
		this.account = parameter;
	}

	public String getAccount() {
		return account;
	}

	public void setName(String parameter) {
		this.name = parameter;
	}
	public String getName() {
		return name;
	}

	public void setPassword(String parameter) {
		this.password = parameter;
	}
	public String getPassword() {
		return password;
	}

	public void setPassword2(String parameter) {
		this.password2 = parameter;
	}
	public String getPassword2() {
		return password2;
	}

	public void setBranch(int branchId) {
		this.branchId = branchId;
	}
	public int getBranch() {
		return branchId;
	}

	public void setDepartment(int departmentId) {
		this.departmentId = departmentId;
	}
	public int getDepartment() {
		return departmentId;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public int getIsStopped() {
		return isStopped;
	}
	public void setIsStopped(int isStopped) {
		this.isStopped = isStopped;
	}
	public boolean matches(String string) {

		return false;
	}

}
