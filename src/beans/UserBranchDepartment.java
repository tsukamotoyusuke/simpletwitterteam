package beans;

import java.util.Date;

public class UserBranchDepartment {

	private int id;
	private String account;
	private String name;
	private String branchName;
	private String departmentName;
	private Date createdDate;
	private int isStopped;

	public int getId() {
		return id;
	}
	public void setId(int parameter) {
		this.id = parameter;
	}

	public String getAccount() {
		return account;
	}
	public void setAccount(String parameter) {
		this.account = parameter;
	}

	public String getName() {
		return name;
	}
	public void setName(String parameter) {
		this.name = parameter;
	}

	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String parameter) {
		this.branchName = parameter;
	}

	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String parameter) {
		this.departmentName = parameter;
	}

	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public int getIsStopped() {
		return isStopped;
	}
	public void setIsStopped(int parameter) {
		this.isStopped = parameter;
	}

}

