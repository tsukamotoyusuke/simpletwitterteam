///*
package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

//ユーザー管理画面、ユーザー編集画面、ユーザー新規登録画面にアクセスした時
@WebFilter( filterName = "managementFilter" , urlPatterns = {"/management", "/setting", "/signup"})
public class ManagementFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletResponse responses = (HttpServletResponse) response;
		List<String> errorMessages = new ArrayList<String>();

		//HttpSessionオブジェクトを取得する
		HttpSession session = ((HttpServletRequest)request).getSession();

		User loginUser = (User) session.getAttribute("loginUser");

		//もしログインしているユーザーの(支社は本社ではない)かつ(部署が総務人事部ではない)場合
		if( (loginUser.getBranch() != 1) && (loginUser.getDepartment() != 1) ) {

			errorMessages.add("権限がありません");
			session.setAttribute("errorMessages", errorMessages);
			responses.sendRedirect("./");

		} else {
			//正常に実行
			chain.doFilter(request, response); // サーブレットを実行
		}
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}
