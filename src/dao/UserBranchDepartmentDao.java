package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;
import exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	public List<UserBranchDepartment> select(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("    users.id as id, ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as name, ");
			sql.append("    branches.name as branch_name,");
			sql.append("    departments.name as department_name, ");
			sql.append("    users.created_date as created_date, ");
			sql.append("    users.is_stopped as is_stopped ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> managements = toManagements(rs);
			return managements;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserBranchDepartment> toManagements(ResultSet rs) throws SQLException {

		List<UserBranchDepartment> managements = new ArrayList<UserBranchDepartment>();
		try {
			while (rs.next()) {
				UserBranchDepartment management = new UserBranchDepartment();
				management.setId(rs.getInt("id"));
				management.setAccount(rs.getString("account"));
				management.setName(rs.getString("name"));
				management.setBranchName(rs.getString("branch_name"));
				management.setDepartmentName(rs.getString("department_name"));
				management.setCreatedDate(rs.getTimestamp("created_date"));
				management.setIsStopped(rs.getInt("is_stopped"));

				managements.add(management);
			}
			return managements;
		} finally {
			close(rs);
		}
	}
}
