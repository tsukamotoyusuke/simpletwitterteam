package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Departments;
import exception.SQLRuntimeException;

public class DepartmentDao {

	public List<Departments> select(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT id, name FROM departments ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<Departments> departments = toDepartments(rs);
			return departments;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Departments> toDepartments(ResultSet rs) throws SQLException {

		List<Departments> departments = new ArrayList<Departments>();
		try {
			while (rs.next()) {
				Departments department = new Departments();

				department.setId(rs.getInt("id"));
				department.setName(rs.getString("name"));

				departments.add(department);

			}
			return departments;
		} finally {
			close(rs);
		}
	}
}