package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Departments;
import dao.DepartmentDao;

public class DepartmentService {

	public List<Departments> select() {

		final int LIMIT_NUM = 1000;
		Connection connection = null;
		try {
			connection = getConnection();
			List<Departments> departments = new DepartmentDao().select(connection, LIMIT_NUM);

			commit(connection);

			return departments;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}