package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branches;
import dao.BranchDao;

public class BranchService {

	public List<Branches> select() {

		final int LIMIT_NUM = 1000;
		Connection connection = null;
		try {
			connection = getConnection();
			List<Branches> branches = new BranchDao().select(connection, LIMIT_NUM);

			commit(connection);

			return branches;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}